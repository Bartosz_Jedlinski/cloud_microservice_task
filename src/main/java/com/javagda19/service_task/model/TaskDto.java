package com.javagda19.service_task.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {
    private Long ownerId;

    private String description;


    public static Task fromDto(TaskDto taskDto) {
        return new Task(taskDto.getOwnerId(), taskDto.getDescription());
    }
}
