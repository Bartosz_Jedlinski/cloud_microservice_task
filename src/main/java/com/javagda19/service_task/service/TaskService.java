package com.javagda19.service_task.service;

import com.javagda19.service_task.apiclient.UserApiClient;
import com.javagda19.service_task.model.AppUserDto;
import com.javagda19.service_task.model.Task;
import com.javagda19.service_task.model.TaskDto;
import com.javagda19.service_task.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;



@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private UserApiClient userApiClient;

    public Long add(TaskDto taskDto) {
        Task newlyCreated = TaskDto.fromDto(taskDto);
        try {
            AppUserDto dto = userApiClient.get(taskDto.getOwnerId());

            newlyCreated = taskRepository.save(newlyCreated);

            return newlyCreated.getId();
        }catch(Exception e){
            System.err.println();
        }
        throw new EntityNotFoundException("User owner can't be find");
    }

    public List<Task> getByUser(long ownerId) {
        List<Task> tasks = taskRepository.findAllByOwnerId(ownerId);
        return tasks;
    }

    public Task getOne(long taskId) {
        Optional<Task> tasksOpt = taskRepository.findById(taskId);
        return tasksOpt.orElseThrow(() -> new EntityNotFoundException());
    }

}
